// Main file for WiBotic QEMU ARM workspace
// Based on https://balau82.wordpress.com/2010/02/28/hello-world-for-bare-metal-arm-using-qemu/
// and https://balau82.wordpress.com/2012/04/15/arm926-interrupts-in-qemu/

#include <cstdint>

// See: http://infocenter.arm.com/help/topic/com.arm.doc.ddi0183f/DDI0183.pdf
// Also: http://infocenter.arm.com/help/topic/com.arm.doc.ddi0287b/DDI0287B_arm926ejs_dev_chip_technical_reference_manual.pdf
volatile void *const UART0BASE = (void *)0x101f1000;
volatile char *const UART0DR = (char *)UART0BASE;
volatile uint16_t *const UART0IMSC = (uint16_t *)((char *)UART0BASE + 0x38);
volatile uint16_t *const UART0MIS = (uint16_t *)((char *)UART0BASE + 0x40);
volatile uint16_t *const UART0ICR = (uint16_t *)((char *)UART0BASE + 0x44);

volatile void *const UART1BASE = (void *)0x101f2000;
volatile char *const UART1DR = (char *)UART1BASE;
volatile uint16_t *const UART1IMSC = (uint16_t *)((char *)UART1BASE + 0x38);
volatile uint16_t *const UART1MIS = (uint16_t *)((char *)UART1BASE + 0x40);
volatile uint16_t *const UART1ICR = (uint16_t *)((char *)UART1BASE + 0x44);

volatile void *const VICBASE = (void *)0x10140000;
volatile uint16_t *const VICINTENABLE = (uint16_t *)((char *)VICBASE + 0x10);

volatile void *const TIMERBASE = (void *)0x101e2000;
volatile uint32_t *const TIMER0 = (uint32_t *)TIMERBASE;
volatile uint32_t *const TIMER0VALUE = (uint32_t *)((char *)TIMERBASE + 0x04);
volatile uint8_t *const TIMER0CONTROL = (uint8_t *)((char *)TIMERBASE + 0x08);
volatile uint8_t *const TIMER0MIS = (uint8_t *)((char *)TIMERBASE + 0x14);
volatile uint32_t *const TIMER0CLEAR = (uint32_t *)((char *)TIMERBASE + 0x0C);

void print_uart0(const char *s)
{
  while (*s != '\0')
  {
    *UART0DR = *s;
    s++;
  }
}

void enable_uart0_irq(void)
{
  *VICINTENABLE = 0x1000; // Enable UART0 IRQ
  *UART0IMSC = 0x10; // Enable RXIM on UART0 IRQ
}

void enable_timer_irq(void)
{
  *VICINTENABLE = 0x10; // Enable TIMER0 IRQ
  *TIMER0 = 1000000; // QEMU Ticks at 1MHz
  *TIMER0CONTROL = 0xE2; // Enable timer as 32bit Periodic with interrupts
}

extern "C"
{
  int main()
  {
    enable_uart0_irq();
    enable_timer_irq();
    print_uart0("Hello world!\n");
    while (1) {
    }
  }

  void __attribute__((interrupt)) irq_handler()
  {
    if (*TIMER0MIS)
    {
      print_uart0("Tick\n");
      *TIMER0CLEAR = 1;
    }
    if (*UART0MIS)
    {
      // Data reads clear the UART0 RXIM interrupt
      const char data_in = *UART0DR; 
      char buffer[] = "You sent:  \n";
      buffer[10] = data_in;
      print_uart0(buffer);
    }
  }

  void __attribute__((interrupt)) default_handler(void)
  {
    // Do Nothing
  }

  void copy_vectors(void)
  {
    extern uint32_t vectors_start;
    extern uint32_t vectors_end;
    uint32_t *vectors_src = &vectors_start;
    uint32_t *vectors_dst = (uint32_t *)0;

    while (vectors_src < &vectors_end)
      *vectors_dst++ = *vectors_src++;
  }
}
.PHONY: clean all run rundebug

all: app.bin

run: app.bin
	#Press CTRL+A x to terminate
	qemu-system-arm -M versatilepb -m 512K -display none -serial stdio -serial pty -kernel app.bin

rundebug: app.bin
	#Connect arm-none-eabi-gdb on :1234 to start
	qemu-system-arm -M versatilepb -m 512K -serial stdio -s -S -kernel app.bin

startup.o: startup.s
	arm-none-eabi-as -mcpu=arm926ej-s -g startup.s -o startup.o

main.o: main.cpp
	arm-none-eabi-g++ -c -mcpu=arm926ej-s -g main.cpp -o main.o

app.elf: startup.o main.o main.ld
	arm-none-eabi-ld -T main.ld main.o startup.o -o app.elf

app.bin: app.elf
	arm-none-eabi-objcopy -O binary app.elf app.bin

clean:
	rm *.bin *.elf *.o